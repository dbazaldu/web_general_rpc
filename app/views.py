from flask import render_template
from app import app
from .database import db as cern_db
from flask import Response
from flask import request
import json


@app.route('/')
def index():
    return render_template("index.html", title="Home")


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/api/geometry', methods=['GET', 'POST'])
def geometry():
    if request.method == 'GET':
        geometry_map = cern_db.get_geometry()
        resp = Response(json.dumps(geometry_map), status=200,
                        mimetype='application/json')
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp
    if request.method == 'POST':
        content = request.json
        resultset = cern_db.get_uids(content['geometry'])
        return Response(json.dumps(resultset), status=200, mimetype='application/json')


@app.route('/api/test', methods=['GET', 'POST'])
def data_test():
    if request.method == 'GET':
        test_data = cern_db.get_test_data()
        resp = Response(json.dumps(test_data), status=200,
                        mimetype='application/json')
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp
    if request.method == 'POST':
        content = request.json
        resultset = cern_db.get_test_data(content['dpids'][0], content['dates']['start_date'], content['dates']['end_date']) 
        return Response(json.dumps(resultset), status=200, mimetype='application/json')