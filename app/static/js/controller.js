new Vue({
    store: store,
    el: '#app',
    data: function(){
        return{
          panels:{
            panel1: this.$store.state.panels.panel1.preset,
            panel2: this.$store.state.panels.panel2.preset,
            panel3: this.$store.state.panels.panel3.preset
          },
          uuids: [],
          plot: {
            data:[],
            layout: {
              title:'EXAMPLE PLOT',
              xaxis: {
                  title: 'Time'
              },
              yaxis: {
                  title: 'IMON'
              },
              height: 700,
              showline: true,
              showgrid: true
            },
            options: {
              displaylogo: false
            },
            sql: ''
          }
        }
      },
      beforeCreate() {
        this.$store.dispatch('get_geometry');
      },
      created(){
        // axios.get('/api/test')
        //     .then(r => {
        //         var data = {
        //             x: null,
        //             y: null,
        //             type: '',
        //             marker: null
        //         }
        //         data['x'] = r.data.x;
        //         data['y'] = r.data.y;
        //         data['type'] = 'line';
        //         data['marker'] = {
        //             color: '#2d88fe'
        //         };
        //         this.plot.data.push(data);
        //         this.plot.sql = r.data.sql;
        // });
      },
      filters:{
        normal_type: function(text){
          let init = text.replace('-', " ");
          let temp = init.charAt(0).toUpperCase() + init.slice(1);
          return temp;
        }
      },
      computed: {
        example_data(){
          if( this.plot.data.length != 0 )
              return this.plot.data;
          return []
        },
        select_all:{
          // Original idea took from: https://stackoverflow.com/questions/33571382/check-all-checkboxes-vuejs
          get: function () {
            return this.$store.getters.get_uuids ? this.uuids.length == this.$store.getters.get_uuids.length : false;
          },
          set: function (value) {
            var selected = [];
            if (value) {
                this.$store.getters.get_uuids.forEach(function (uuid) {
                    selected.push(uuid);
                });
            }
            this.uuids = selected;
          }
        },
        cached_uuids: function(){
          let uuids = this.$store.getters.get_uuids;
          if (uuids.length != 0) {
            return uuids;
          }
          return [];
        },
        barrelMap(){
          if (this.$store.getters.get_geometry != null)
            return this.$store.getters.get_geometry['b'];
        },
        endcapMap(){
          if (this.$store.getters.get_geometry != null)
          return this.$store.getters.get_geometry['e'];
        }
      },
      methods: {
        get_options(){
            return this.$store.getters.get_options_panel3;
        },
        changePanel(panel_name, origin){
          this.panels[origin] = panel_name;
          this.$store.commit('updatePanel', {
            panel: origin,
            change: panel_name
          });
        },
        update_geometry(data){
          var them_all = data;
          for (const el in them_all) {
            let _id = el;
            this.$store.commit('changeProp', {
              super: 'geometry', 
              parent: this.panels.panel1, 
              child: _id,
              value: them_all[el].selected
              });
          }
        },
        send_request(){ // This should be raised when user clicks at submit button
          let geometry = this.$store.getters[this.panels.panel1];
          geometry['rg'] = 'e'
          if (this.panels.panel1 == 'barrel')
            geometry['rg'] = 'b'
          
          axios.post('/api/geometry', {geometry: geometry})
            .then(r => r.data)
            .then(uuids => {
              // console.log(uuids);
              this.$store.commit('update_uuids', uuids);
            });
        },
        // Second Panel
        timeline_change(data){
            for (const key in data.value) {
                this.$store.commit('changeProp', {
                    super: 'timeline', 
                    parent: data.parent, 
                    child: key,
                    value: data.value[key]
                });
            }
        },
        get_data_with_dpid(){
          let selected = this.uuids;
          let dates = this.$store.getters[this.panels.panel2];
          let dpids = [];
          for (const el in selected) {
            dpids.push(selected[el][1]);
          }
          console.log(dpids);
          
          axios.post('/api/test', {
            dpids: dpids,
            dates: dates
          })
              .then(r => {
                  var data = {
                      x: null,
                      y: null,
                      type: '',
                      marker: null
                  }
                  data['x'] = r.data.x;
                  data['y'] = r.data.y;
                  data['type'] = 'scatter';
                  data['mode'] = 'markers';
                  data['marker'] = {
                      // color: '#2d88fe'
                  };
                  this.plot.data.push(data);
                  this.plot.sql = r.data.sql;
          });
        },
        // Third panel
        optionChanged( data ){
          // TODO: Method to retrieve the data. 
          console.log(data);
        }
      },
      components: {
        'vue-plotly': vueplotly,
        'geometry': Geometry,
        'datepicker': datepicker,
        'run': run,
        'il': il,
        'no-filter': no_filter,
        'standard': standard,
        'expert': expert,
        'advanced': advanced
    }
});