#!/usr/bin/env python
import cx_Oracle
import json
import uuid

# TODO: Change this into a ENV Variable.
CONNECTION_STD = 'CMS_RPC_TEST_R/rpcr17d3R@int2r'

QUERY_MAP = """
SELECT DISTINCT
  REGION,
  WHEEL_DISK,
  RING_STATION,
  SECTOR,
  utl_raw.cast_to_nvarchar2(listagg(utl_raw.cast_to_raw(POSITION), utl_raw.cast_to_raw(N',')) within group (order by POSITION)) as POSITION
FROM CMS_RPC_PVSS_TEST.RPC_CHAMBERS
GROUP BY REGION, WHEEL_DISK, RING_STATION, SECTOR
ORDER BY REGION, WHEEL_DISK, RING_STATION, SECTOR, POSITION
"""

QUERY_TABLES = """
'SELECT owner, table_name FROM all_tables'
"""
USER = 'CMS_RPC_TEST_R'
PASS = 'rpcr17d3R'
TNS_name = 'int2r'

class Oracle(object):
    
    def __init__(self, user, password, TNS_name):
        self.user = user
        self.password = password
        self.TNS_name = TNS_name

    def connect(self):
        """ Connect to the database. """

        try:
            self.db = cx_Oracle.connect(self.user + '/' + self.password + '@' + self.TNS_name)
        except cx_Oracle.DatabaseError:
            raise

        self.cursor = self.db.cursor()

    def disconnect(self):
        """
        Disconnect from the database. If this fails, for instance
        if the connection instance doesn't exist, ignore the exception.
        """

        try:
            self.cursor.close()
            self.db.close()
        except cx_Oracle.DatabaseError:
            pass

    def execute(self, sql, bindvars=None):
        """
        Execute whatever SQL statements are passed to the method;
        commit if specified. Do not specify fetchall() in here as
        the SQL statement may not be a select.
        bindvars is a dictionary of variables you pass to execute.
        """
        try:
            if bindvars == None:
                self.cursor.execute(sql)
            else:
                self.cursor.execute(sql, bindvars)
        except cx_Oracle.DatabaseError:
            # Log error as appropriate
            raise


# Geometry Stuff
def update_tree(elements: list, mapped_info: dict):
    """
    For a dictionary initialized before, fills the information 
    of the elements in the database.

    Args:
        elements: The list of (region, wheel_disk, ring_station, sector, position).
        mapped_info: Dictionary where all the options are going to be available. 

    Returns:
        Dict mapped_info updated.

    """
    region = elements[0]
    wd = elements[1]
    rs = elements[2]
    se = elements[3]
    po = elements[4]

    if region not in mapped_info:
        mapped_info[region] = {
            wd: {
                rs: {
                    se: po
                }
            }
        }
    else:
        if wd not in mapped_info[region]:
            mapped_info[region][wd] = {
                rs: {
                    se: po
                }
            }
        else:
            if rs not in mapped_info[region][wd]:
                mapped_info[region][wd][rs] = {
                    se: po
                }
            else:
                mapped_info[region][wd][rs][se] = po

    return mapped_info


def generate_map(chunk):
    """
    Check the information and fills the map.
    Args:
        chunk: Raw information of the map.
    """
    mapped_info: dict = {}
    positions: list = []
    for ele_in in chunk:
        ele_in = list(ele_in)
        if ele_in[4] is not None:
            positions = ele_in[4].split(',')
            ele_in[4] = positions
        mapped_info = update_tree(ele_in, mapped_info)

    return mapped_info


def get_geometry():

    oracle = Oracle(USER, PASS, TNS_name)
    oracle.connect()
    try:
        oracle.execute(QUERY_MAP)
        resultset = oracle.cursor.fetchall()
        final_map = generate_map(resultset)

        return final_map

    finally:
        oracle.disconnect()


def get_uids(options):
    sql = prepare_query(options)
    oracle = Oracle(USER, PASS, TNS_name)
    oracle.connect()
    try:
        print (sql)
        oracle.execute(sql)
        resultset = oracle.cursor.fetchall()
        print (oracle.cursor.description)

    finally:
        oracle.disconnect()

    print(sql)
    #  In case of UUIDS
    new_resultset = []
    for el in resultset:
        u = list(el)[0]
        temp = uuid.UUID(bytes=u)
        new_resultset.append([str(temp), el[1]])

    return new_resultset
    

def prepare_query(options):
    print ("Desde adentro", options['po'])
    SQL = """
    SELECT DISTINCT
        UUID,
        DPID
    FROM CMS_RPC_PVSS_TEST.RPC_CHAMBERS chambers
    left join CMS_RPC_PVSS_TEST.RPC_DPID_LOG dpid
    ON chambers.UUID = dpid.COMPONENT
    """
    if options['rg'] != 'ALL':
        SQL += "\nWHERE REGION = '" + options['rg'] + "'"
    if options['wd'] != 'ALL':
        SQL += '\nAND WHEEL_DISK = ' + options['wd']
    if options['rs'] != 'ALL':
        SQL += '\nAND RING_STATION = ' + options['rs']
    if options['se'] != 'ALL':
        SQL += '\nAND SECTOR = ' + options['se']
    if options['po'] != 'ALL':
        if options['po'] != 'None':
            SQL += "\nAND POSITION = '" + options['po'] + "'"
        else: 
            SQL += "\nAND POSITION is NULL"

    SQL += """\n
AND TYPE = 'HV' 
"""

    print (SQL)
    return SQL

def get_test_data(DPID, start_date, end_date):
    USER = 'CMS_RPC_TEST_R'
    PASS = 'rpcr17d3R'
    TNS_name = 'int2r'
    SQL = """
    SELECT
    IMON,
    CHANGE_DATE
    FROM CMS_RPC_PVSS_TEST.RPCCURRENTS
    WHERE DPID=""" + str(DPID) + """
    AND CHANGE_DATE
    BETWEEN TO_TIMESTAMP( '""" + start_date + """ 00:00:00', 'YYYY.MM.DD HH24:MI:SS')
    AND TO_TIMESTAMP( '""" + end_date + """ 22:59:59', 'YYYY.MM.DD HH24:MI:SS')
    ORDER BY CHANGE_DATE
    """
    oracle = Oracle(USER, PASS, TNS_name)
    oracle.connect()
    try:
        # No commit as you don-t need to commit DDL.
        oracle.execute(SQL)
        resultset = oracle.cursor.fetchall()
        new_resultset = {
            'x': [],
            'y': [],
            'sql': SQL
        }
        for el in resultset:
            current = el[0]
            date = el[1].strftime("%Y-%m-%d %H:%M:%S")
            new_resultset['x'].append(date)
            new_resultset['y'].append(current)

    finally:
        oracle.disconnect()
        return new_resultset
    

def test1():
    USER = 'CMS_RPC_TEST_R'
    PASS = 'rpcr17d3R'
    TNS_name = 'int2r'
    oracle = Oracle(USER, PASS, TNS_name)
    oracle.connect()
    try:
        # No commit as you don-t need to commit DDL.
        oracle.execute(QUERY_MAP)
        resultset = oracle.cursor.fetchall()
        final_map = generate_map(resultset)

        print (final_map)

    finally:
        oracle.disconnect()


if __name__ == '__main__':
    test1()
