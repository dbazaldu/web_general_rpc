# Useful commands for OpenShift

```shell
oc new-app ./ --env-file=./.env/production.env --strategy=source
```

$ oc - openshift-cli

$1 new-app - command to create a new instance in the openshift cloud

$2 ./ - path to the app

$3 --env-file - Need a enviornment file to create the build and deploy. In this case we are using gunicorn for the WSGI server. Flask provide a file to run the app which is run.py so the command to gunicorn will be:

```shell
$ gunicorn --bind 0.0.0.0:5000 run:app
```

$ --strategy - Amazing command that detects which programming language you are using and makes the deploy using that information. 