import Vue from 'vue'
import App from './App.vue'
import Plotly from './Plotly.vue'
import Vuex from 'vuex'


Vue.use(Vuex)

// This names may change if one component name is different
const BARREL = 'barrel';
const ENDCAP = 'endcap';
const DATE = 'datepicker';
const RUN = 'run';
const INT_LUMI = 'il';
const NO_FILTER = 'no-filter';
const STD = 'standard';
const ADV = 'advanced';
const EXP = 'expert';

// Everything singular should be singular (language speaking)
const store = new Vuex.Store({
  state: {
    raw_geometry: null,
    panels: { // This names represent the name of the registered components in the App.vue class
      panel1: {
        preset: BARREL,
        options: [BARREL, ENDCAP]
      },
      panel2:{
        preset: DATE,
        options: [DATE, RUN, INT_LUMI]
      },
      panel3: {
        preset: NO_FILTER,
        options: [NO_FILTER, STD, ADV, EXP]
      }
    },
    geometry: {
      barrel: {
        wd: 'ALL',
        rs: 'ALL',
        se: 'ALL',
        po: 'ALL'
        },
      endcap:{
        wd: 'ALL',
        rs: 'ALL',
        se: 'ALL',
        po: 'None'
      },
      uuids: []
    },
    timeline:{
      datepicker: {
        start_date: '2009-10-01',
        end_date: 'today'
      },
      run:{
        start_run: 0,
        end_run: 0
      },
      il: {
        il: 0
      }
    }
  },
  getters: {
    panels_status(state){
      return {
        panel1: state.panels.panel1.preset,
        panel2: state.panels.panel2.preset,
        panel3: state.panels.panel3.preset
      }
    },
    panel1(state){
      return state.panels.panel1.preset;
    },
    panel2(state){
      return state.panels.panel2.preset;
    },
    panel3(state){
      return state.panels.panel3.preset;
    },
    barrel(state) {
      return state.geometry.barrel
    },
    endcap(state){
      return state.geometry.endcap
    },
    datepicker(state){
      return state.timeline.datepicker
    },
    il(state){
      return state.timeline.il
    },
    get_geometry(state){
      return state.raw_geometry;
    },
    get_uuids(state){
      return state.geometry.uuids;
    },
    get_options_panel3(state){
      return state.panels.panel3.options;
    }
  },
  mutations:{
    changeProp( state, payload ){
      // console.log("Change: " + payload.super + '/' + payload.parent + '/' + payload.child + '/' + payload.value);
      state[payload.super][payload.parent][payload.child] = payload.value;
    },
    updatePanel( state, payload ){
      state.panels[payload.panel].preset = payload.change;
    },
    SET_RAW_GEOMETRY: function(state, geometry){
      state.raw_geometry = geometry;
    },
    update_uuids: function(state, uuids) {
      // console.log(uuids);
      
      state.geometry.uuids = uuids;
    }
  },
  actions: {
    get_geometry: function({ commit }){
      axios.get('http://localhost:5000/api/geometry')
        .then(r => r.data)
        .then(geometry => {
          commit('SET_RAW_GEOMETRY', geometry);
        });
    }
  }
  
});

Vue.config.productionTip = false

// new Vue({
//   store,
//   render: h => h(App)
// }).$mount('#app')

new Vue({
  render: h => h(Plotly)
}).$mount('#app')
