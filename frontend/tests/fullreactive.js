var fs = require('fs');
var _ = require('lodash');

var obj = JSON.parse(fs.readFileSync('example.json', 'utf8'));

just_test('-1', 'ALL', 'ALL', 'ALL');


function just_test(_wd, _rs, _se, _po){
    let res = retrieve_selective({
        wd: {
            selected: _wd,
        },
        rs: {
            selected: _rs,
        },
        se: {
            selected: _se
        },
        po: {
            selected: _po
        }
    });
    console.log(res);
    
}

function retrieve_selective(arr) {
    var _map = obj['b'];
    let ALL = 'ALL';
    let bwd = arr.wd.selected == ALL ? 0 : 4;
    let brs = arr.rs.selected == ALL ? 0 : 2;
    let bse = arr.se.selected == ALL ? 0 : 1;
    let mask = bwd + brs + bse;
    
    let wd = arr.wd.selected, 
        rs = arr.rs.selected,
        se = arr.se.selected,
        po = arr.po.selected;

    let cached;

    switch(mask){
        case 0:
            // This is the default state
            cached = GET_DISTINCT_O1(0, null);
            return cached;
            
        break;
        case 1:
            // Carry when its position null
            return GET_DISTINCT_O1(1, [se]);
        break;
        case 2:
            return GET_DISTINCT_23(2, [rs]);
        break;
        case 3:
            return GET_DISTINCT_23(3, [rs, se]);
        break;
        case 4:
            return GET_DISTINCT_45(4, [wd]);
        break;
        case 5:
            return GET_DISTINCT_45(5, [wd, se]);
        break;
        case 6:
            return GET_DISTINCT_67(6, [wd, rs]); 
        break;
        case 7:
            return GET_DISTINCT_67(7, [wd, rs, se]); 
        break;
        default:
        break;
    }
}


function GET_DISTINCT_67(flag, values){
    var _map = obj['b'];

    var wheels_disks = [],
        ring_station = [],
        sector = [],
        positions = [];

    let wd = values[0], 
        rs = values[1];
    
    if ( flag == 6 ){
        for (const el in _map[wd][rs]){
            sector.push(el);
            if(_map[wd][rs][el] != null){
                for (let index = 0; index < (_map[wd][rs][el]).length; index++) {
                    positions.push(_map[wd][rs][el][index]);
                }
            }
            else{
                positions.push('None');
            }
        }
    }
    else{
        let se = values[2];
        sector.push(values[2]);
        if (_map[wd][rs][se] != null) {
            positions = _map[wd][rs][se];
        }
        else{
            positions.push('None');
        }
        
    }

    wheels_disks.push(wd),
    ring_station.push(rs)

    return {
        wd: _.uniq(wheels_disks), 
        rs: _.uniq(ring_station), 
        se: _.uniq(sector), 
        po: _.uniq(positions)
    }
    

}

function GET_DISTINCT_45(flag, values){
    var _map = obj['b'];

    var wheels_disks = [],
        ring_station = [],
        sector = [],
        positions = [];
    
    wheels_disks = [values[0]];

    let el = values[0];
    for (const l2 in _map[el]) {
        ring_station.push(l2);
        if( flag == 4 ){
            for (const l3 in _map[el][l2]) {
                sector.push(l3);
                if(_map[el][l2][l3] != null){
                    for (let index = 0; index < (_map[el][l2][l3]).length; index++) {
                        positions.push(_map[el][l2][l3][index]);
                    }
                }
                else{
                    positions.push('None');
                }
            }
        }
        if( flag == 5){
            sector = values[1];
            if(_map[el][l2][sector] != null){
                for (let index = 0; index < (_map[el][l2][sector]).length; index++) {
                    positions.push(_map[el][l2][sector][index]);
                }
            }
            else{
                positions.push('None');
            }
        }
        
    }

    return {
        wd: _.uniq(wheels_disks), 
        rs: _.uniq(ring_station), 
        se: _.uniq(sector), 
        po: _.uniq(positions)
    }


}

function GET_DISTINCT_O1(flag, values){
    var _map = obj['b'];

    var wheels_disks = [],
        ring_station = [],
        sector = [],
        positions = [];

    
    for (const el in _map) {
        wheels_disks.push(el);
        for (const l2 in _map[el]) {
            ring_station.push(l2);
            if( flag == 0 ){
                for (const l3 in _map[el][l2]) {
                    sector.push(l3);
                    if(_map[el][l2][l3] != null){
                        for (let index = 0; index < (_map[el][l2][l3]).length; index++) {
                            positions.push(_map[el][l2][l3][index]);
                        }
                    }
                    else{
                        positions.push('None');
                    }
                }
            }
            if( flag == 1){
                sector.push(values[0]);
                if(_map[el][l2][values[0]] != null){
                    for (let index = 0; index < (_map[el][l2][values[0]]).length; index++) {
                        positions.push(_map[el][l2][values[0]][index]);
                    }
                }
                else{
                    positions.push('None');
                }
            }
            
        }
    }

    return {
        wd: _.uniq(wheels_disks), 
        rs: _.uniq(ring_station), 
        se: _.uniq(sector), 
        po: _.uniq(positions)
    }
    
}

function GET_DISTINCT_23(flag, values){

    var wheels_disks = [],
        ring_station = [],
        sector = [],
        positions = [];


    let rs = values[0];
    
    ring_station.push(rs);
    for (const el in _map) {
        wheels_disks.push(el);
        if (flag == 2){
            for (const key in _map[el][rs]) {
                sector.push(key);
                if(_map[el][rs][key] != null){
                    for (let index = 0; index < (_map[el][rs][key]).length; index++) {
                        positions.push(_map[el][rs][key][index]);
                    }
                }
                else{
                    positions.push('None');
                }
            }
        }
        else{
            sector = values[1];
            if(_map[el][rs][sector] != null){
                for (let index = 0; index < (_map[el][rs][sector]).length; index++) {
                    positions.push(_map[el][rs][sector][index]);
                }
            }
            else{
                positions.push('None');
            }
        }
        
    }

    return {
        wd: _.uniq(wheels_disks), 
        rs: _.uniq(ring_station), 
        se: _.uniq(sector), 
        po: _.uniq(positions)
    }

}
